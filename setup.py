from setuptools import setup

setup(
    name='humitos',
    version='0.0.1',
    description='description',
    long_description='long description',
    classifiers=[
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.5',
    ],
    author='humitos',
    author_email='humitos@gmail.com',
    zip_safe=False,
    extras_require={
        'docs': [
            'Sphinx',
            'recommonmark',
            'sphinx-rtd-theme',
            'sphinxcontrib-spelling',
        ],
    },
)
